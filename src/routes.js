module.exports = function(app) {
  
  var c = app.controllers,
      m = app.middlewares,
      log = m.LogMiddleware;

  // To get all routes
  app.route('*')
    .get(log.before)
    .post(log.before);

  // Log routes
	var logC = new c.log.LogsController();

	app.route('/logs')
		.get(logC.index)
        .post(logC.create);

  // Home routes
  var home = new c.home.HomeController();

  app.route('/')
    .get(home.index);

  //login routes
  var login = new c.login.LoginController();
  app.route('/login')
      .post(login.login);

  // Acts routes
  var act = new c.act.ActController();

  app.route('/acts')
    .get(act.index)
    .post(act.store);

  app.route('/acts/:id')
    .put(act.evaluate);

  //user routes
  var user = new c.user.UserController();

  app.route('/users')
      .get(user.index)
      .post(user.store);

};