var express = require('express');
var load = require('express-load');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var morgan = require('morgan');
var _ = require('lodash');
var CryptoHelper = require('../src/Helpers/CryptoHelper')();

module.exports = function() {

    var crypt = CryptoHelper.getInstance();

    var app = express();

    var APP_PORT = process.env.PORT || 4000;

    app.set('port', APP_PORT);

    // novos middlewares
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
    app.use(require('method-override')());
    app.use(cookieParser());

    app.use(session({
        secret: crypt.encrypt('$98as6q9eoi23ne$4qwej1121lkj234lk13251pwdsfd0g2er1wlnflsdnl1kn2sdgtetj2ee76urtygdsfwdfd'),
        resave: true,
        saveUninitialized: true
    }));
    
    // app.use(passport.session());

    app.use(morgan('dev'));

    app.use(function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
        next();
    });

    load('models', { cwd: 'src' })
        .then('helpers')
        .then('middlewares')
        .then('controllers')
        .then('routes')
        .into(app);

    app.get('*', function(req, res, next) {
        res.status(404).json({
            message: 'Route not found!'
        });
        next();
    });

    return app;
};