app.factory('actFactory', function($resource) {
    return $resource('http://localhost:4000/acts/:_id', {
    		_id: '@_id'
    	}, {
    		'update': {
    			method: 'PUT'
    		} 
    	});
});